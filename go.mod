module poc-golang

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.9
	github.com/pkg/errors v0.9.1
	github.com/urfave/negroni v1.0.0
	gopkg.in/redis.v4 v4.2.4
)

require (
	github.com/StevenACoffman/grace v0.0.0-20200103221753-99673b6bade7 // indirect
	github.com/TV4/graceful v0.3.6 // indirect
	github.com/garyburd/redigo v1.6.4 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.27.8 // indirect
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9 // indirect
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20170922094635-f56db5e73a5e // indirect
)
